# My project's README

## Demo Components

1. Request Creator
 * Create "requests" with a given resource (/like/this/url), wait time, and timestamp for when it was created
 * Send requests to DMZ
2. Authenticator and Task Assigner
 * "Authenticate" requests based on format of resource (?)
 * Prioritize requests in priority queue based on resource
3. Request Handler
 * Take requests from priority queue and wait for specified period of time.
