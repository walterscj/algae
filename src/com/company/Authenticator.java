package com.company;

import javax.swing.*;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;

public class Authenticator {

    private ArrayList<String> allLakes = new ArrayList<>();
    private static String[] possibleCommands = {"warning", "levelChange", "requestUserData", "requestResearcherData",
            "requestChemicalData", "requestHistoryData", "gatherForecasterData", "gatherBuoyData"};

    public Authenticator() {
        // This mimics the database call for getting all the lakes
        allLakes.add("Alpha");
        allLakes.add("Beta");
        allLakes.add("Charlie");
    }

    /**
     * This method deconstructs the command and makes sure that the command has acceptable values, then passes back an Array of string which contains the command, detail, lake, and data
     * @param fullCommand The full command in a string.  It is formatted like "command/details/lake/data"
     * @return An Array of Strings that hold the full command's values
     */
    public String[] deconstructCommand(String fullCommand){
        String[] returnArray = fullCommand.split("/");
//        String command = fullCommand.substring(0, fullCommand.indexOf('/'));
//        fullCommand = fullCommand.substring(fullCommand.indexOf('/'));
//        String detail = fullCommand.substring(0, fullCommand.indexOf('/'));
//        fullCommand = fullCommand.substring(fullCommand.indexOf('/'));
//        String lake = fullCommand.substring(0, fullCommand.indexOf('/'));
//        fullCommand = fullCommand.substring(fullCommand.indexOf('/'));
//        String data = fullCommand.substring(0, fullCommand.indexOf('/'));
//        String time = fullCommand.substring(fullCommand.indexOf('/'));

        // If any of the command, detail, lake, or data fails then a null String[] is returned to show an error
        if(!(authCommand(returnArray[0]) && authDetail(returnArray[1]) && authLake(returnArray[2]) && authData(returnArray[3]) && authTime(returnArray[4]))) {
            return null;
        }
//        returnArray[0] = command;
//        returnArray[1] = detail;
//        returnArray[2] = lake;
//        returnArray[3] = data;
//        returnArray[4] = time;
        return returnArray;
    }

    /**
     * This method authenticates the command to make sure that the command is actually a command the system can handle
     * @param command The command that is being authenticated
     * @return True if it is an accepted command, false if it is not
     */
    public boolean authCommand(String command) {
        switch (command) {
            case "warning":
            case "levelChange":
            case "requestUserData":
            case "requestResearcherData":
            case "requestChemicalData":
            case "requestHistoryData":
            case "gatherForecasterData":
            case "gatherBuoyData":
                return true;
            default:
                JOptionPane.showMessageDialog(null, "ERROR: Command " + command + " not accepted.","ERROR", JOptionPane.ERROR_MESSAGE);
                return false;
        }
    }

    /**
     * This method authenticates the details in the command to make sure they are acceptable
     * @param detail The details that are being authenticated
     * @return True if details is accepted, False if not
     */
    public boolean authDetail(String detail) {
        if(detail.contains("ERROR")){
            JOptionPane.showMessageDialog(null, "ERROR: Details of command [" + detail + "] not accepted.","ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    /**
     * This method authenticates the lakes to make sure they are in the "database"
     * @param lake The lake that is being authenticated
     * @return True if lake is accepted, False if not
     */
    public boolean authLake(String lake) {
        if(!allLakes.contains(lake)) {
            JOptionPane.showMessageDialog(null, "ERROR: Lake " + lake + " is not a known lake.","ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    /**
     * Authenticates the data
     * Data cannot contain the String "ERROR"
     * @param data The data that is being authenticated
     * @return True if data is accepted, False if not
     */
    public boolean authData(String data) {
        if(data.contains("ERROR")){
            JOptionPane.showMessageDialog(null, "ERROR: Data of command [" + data + "] not accepted.","ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    /**
     * Authenticates the time
     * @param time The time as a String
     * @return True if time is accepted, False if not
     */
    public boolean authTime(String time) {
        return time.matches("\\d+");
//        try{
//            int temp = parseInt(time);
//            return true;
//        }catch(NumberFormatException e){
//            JOptionPane.showMessageDialog(null, "ERROR: Time of command " + time + " not accepted.","ERROR", JOptionPane.ERROR_MESSAGE);
//            return false;
//        }
    }
}
