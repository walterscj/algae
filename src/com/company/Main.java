package com.company;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws InterruptedException {
    	Map<String, Double> map = null;
	    RequestGenerator generator = new RequestGenerator(map);
	    Authenticator auth = new Authenticator();
	    Prioritizer prioritizer = new Prioritizer();
	    RequestHandler handler = new RequestHandler();

	    String[][] commandBlock = new String[100][5];

	    for(int i=0; i<5; i++) {
	        for(int o=0; o<100; o++) {
	            String command = generator.createRandomRequest();
	            String[] commandArray = auth.deconstructCommand(command);
	            commandBlock[o] = commandArray;
            }

            commandBlock = prioritizer.prioritizedTasks(commandBlock);

	        for(String[] command : commandBlock) {
	            handler.handleRequest(command);
            }
            handler.showAverageResponseTimes();
	        Thread.sleep(2000);
        }
    }
}
