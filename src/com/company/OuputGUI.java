package com.company;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Array;

public class OuputGUI {
    private JFrame frame = new JFrame();

    private JPanel topPanel = new JPanel();

    private JPanel levelPanel = new JPanel();
    private JLabel warningLabel = new JLabel("WARNING");
    private JLabel watchLabel = new JLabel("Watch");
    private JLabel safeLabel = new JLabel("Safe");

    private JPanel requestPanel = new JPanel();
    private JLabel requestLabel = new JLabel("Requesting");
    private JTextArea requestText = new JTextArea();
    private JScrollPane requestPane = new JScrollPane(requestText);
    private JScrollBar requestBar = new JScrollBar();

    private JPanel gatherPanel = new JPanel();
    private JLabel gatherLabel = new JLabel("Gathering");
    private JTextArea gatherArea = new JTextArea();
    private JScrollPane gatherPane = new JScrollPane(gatherArea);
    private JScrollBar gatherBar = new JScrollBar();

    private JTextArea averageTime = new JTextArea();


    public OuputGUI(){

    }

    public void createGUI(){
        frame.setTitle("Algae Bloom Demo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(350,350);
        frame.setLayout(new BoxLayout(frame, BoxLayout.PAGE_AXIS));
        topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.LINE_AXIS));

        levelPanel.setLayout(new BoxLayout(levelPanel, BoxLayout.PAGE_AXIS));
        levelPanel.add(warningLabel);
        levelPanel.add(watchLabel);
        levelPanel.add(safeLabel);

        warningLabel.setBackground(Color.lightGray);
        watchLabel.setBackground(Color.lightGray);
        safeLabel.setBackground(Color.lightGray);

        requestPanel.add(requestLabel);
        requestPanel.add(requestPane);
        requestBar.setOrientation(Adjustable.VERTICAL);

        gatherPanel.add(gatherLabel);
        gatherPanel.add(gatherPane);
        gatherBar.setOrientation(Adjustable.VERTICAL);

        frame.add(topPanel);
        frame.add(averageTime);
    }

    public void warning(String[] command){
        warningLabel.setBackground(Color.red);
        watchLabel.setBackground(Color.lightGray);
        safeLabel.setBackground(Color.lightGray);

        JOptionPane.showMessageDialog(null, "Algae bloom detected in lake " + command[2], "ALERT", JOptionPane.WARNING_MESSAGE);
    }

    public void levelChange(String[] command){
        if(command[1].equals("watch")){
            warningLabel.setBackground(Color.lightGray);
            watchLabel.setBackground(Color.green);
            safeLabel.setBackground(Color.lightGray);
        }else if (command[1].equals("safe")){
            warningLabel.setBackground(Color.lightGray);
            watchLabel.setBackground(Color.lightGray);
            safeLabel.setBackground(Color.green);
        }else{
            JOptionPane.showMessageDialog(null, "ERROR: Invalid level change detected", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void  requestUserData(String[] command){
        requestText.append("Requesting data from lake " + command[2] + " for user "+ command[1]);
    }

    public void requestResearcherData(String[] command){
        requestText.append("Requesting data from lake " + command[2] + " for researcher "+ command[1]);
    }

    public void requestChemicalData(String[] command){
        requestText.append("Requesting chemical data from lake " + command[2] + " for researcher "+ command[1]);

    }

    public void requestHistoryData(String[] command){
        requestText.append("Requesting historical data from lake " + command[2] + " for researcher "+ command[1]);

    }

    public void gatherForecasterData(String[] command){
        gatherArea.append("Gathering data from forecaster for lake " + command[2]);
    }

    public void gatherBuoy(String[] command){
        gatherArea.append("Gathering data from buoy for lake " + command[2]);
    }

    public void averageWait(String temp){
        averageTime.append(temp);
    }
}
