package com.company;

import java.util.ArrayList;

public class Prioritizer {
    private ArrayList<String> allLakes = new ArrayList<>();
    private static String[] possibleCommands = {"warning", "levelChange", "requestUserData", "requestResearcherData",
            "requestChemicalData", "requestHistoryData", "gatherForecasterData", "gatherBuoyData"};
    private int AMOUNTCOMMANDS = 100;

    public Prioritizer() {
        // This mimics the database call for getting all the lakes
        allLakes.add("Alpha");
        allLakes.add("Beta");
        allLakes.add("Charlie");
    }

    // Take the allTask and prioritize them by allTasks[n][0] (n and 0)
    public String[][] prioritizedTasks(String[][] allTasks) {
        String[][] orderedTasks = allTasks;
        String[] temp;
        int firstValue = 0, secondValue = 0;
        for(int i = 0; i < AMOUNTCOMMANDS; i++) {
            for(int j = 1; j < AMOUNTCOMMANDS - 1; j++) {
                int firstInt;
                for(firstInt = 0; firstInt < possibleCommands.length; firstInt++) {
                    if(orderedTasks[j-1][0] == possibleCommands[firstInt]){
                        firstValue = firstInt;
                    }
                }
                int secondInt;
                for(secondInt = 0; secondInt < possibleCommands.length; secondInt++) {
                    if(orderedTasks[j][0] == possibleCommands[secondInt]){
                        secondValue = secondInt;
                    }
                }
                if(firstValue > secondValue) {
                    temp = orderedTasks[j-1];
                    orderedTasks[j-1] = orderedTasks[j];
                    orderedTasks[j] = temp;
                }
            }
        }
        return orderedTasks;
    }
}
