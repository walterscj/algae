package com.company;

import com.sun.xml.internal.fastinfoset.util.StringArray;

import java.util.Map;
import java.util.Random;
import java.util.Set;

public class RequestGenerator {
    private Random randomizer = new Random();
    private static String[] possibleCommands = {"warning", "levelChange", "requestUserData", "requestResearcherData",
            "requestChemicalData", "requestHistoryData", "gatherForecasterData", "gatherBuoyData"};
    Map<String, Double> map;

    RequestGenerator(Map<String, Double> map) {
        this.map = map;
    }

    /**
     * This method creates a random request array.
     * @return a random request array.
     */
    public String createRandomRequest() {
        String requestType = getRandomType();
        String error = "";

        if(requestType.equals("random")) {
            requestType = possibleCommands[randomizer.nextInt(8)];
        }

        if(Math.random() < 0.001) {
            error = "ERROR";
        }else {
            error = Integer.toString(randomizer.nextInt(10000));
        }

        return requestType + "/" + error + "/" +
                randomizer.nextInt(10000) + "/" + randomizer.nextInt(8) + "/" + Long.toString(System.currentTimeMillis());
    }

    public String getRandomType() {
        double inert = Math.random() * 2;
        int index = 0;

        String[] keys = Arrays.copyOf(map.keySet().toArray(), map.size(), String[].class);

        while(inert > 0) {
            index = (index + 1) % keys.length;
            inert -= map.get(keys[index]);
        }

        return keys[index];
    }

}
