package com.company;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class RequestHandler {

    private static String[] requestTypes = {"warning", "levelChange", "requestUserData", "requestResearcherData",
            "requestChemicalData", "requestHistoryData", "gatherForecasterData", "gatherBuoyData"};
    private static int NUM_THREADS = 16;
    private ExecutorService threadPool;
    private HashMap<String, Integer> waitTimes;
    private HashMap<String, AtomicLong> numRequests;
    private HashMap<String, AtomicReference<Double>> averageResponseTimes;
    OuputGUI outputGUI = new OuputGUI();

    public RequestHandler(){
        this.threadPool = Executors.newFixedThreadPool(NUM_THREADS);
        this.waitTimes = new HashMap<>();
        this.averageResponseTimes = new HashMap<>();
        this.numRequests = new HashMap<>();
        int waitTime = 10; // Start with 10 ms
        for(String key:requestTypes){
            this.averageResponseTimes.put(key, new AtomicReference<>(0.0));
            this.numRequests.put(key, new AtomicLong(0));
            this.waitTimes.put(key, waitTime);
            waitTime = waitTime * 2;
        }
    }

    public void handleRequest(String[] commandBlock) {
        if(this.threadPool.isShutdown())
            this.threadPool = Executors.newFixedThreadPool(NUM_THREADS);
        this.threadPool.execute(() -> {
            try {
                requestWait(commandBlock);
            } catch (InterruptedException e) { e.printStackTrace(); }
        });
    }

    public void showAverageResponseTimes(){
        this.threadPool.shutdown();
        NumberFormat formatter = new DecimalFormat("#0.00");
        try {
            this.threadPool.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) { e.printStackTrace(); }
        for(String key:this.averageResponseTimes.keySet()){
            outputGUI.averageWait("Average wait time for " + key + " request: " + formatter.format(this.averageResponseTimes.get(key).get()) + " ms.");
//            System.out.printf("Average wait time for \"%s\" request: %.2f ms.\n", key, this.averageResponseTimes.get(key).get());
        }
    }

    private void requestWait(String[] commandBlock) throws InterruptedException {
        String command = commandBlock[0];
        Integer waitTime = this.waitTimes.get(command);
//        System.out.println("Wait time: " + waitTime);
        AtomicLong requests = this.numRequests.get(command);
        long requestNum = requests.getAndIncrement() + 1;
        double wait = waitTime * (1 + 0.2 * Math.random() - 0.1);
//        System.out.println(wait);
        Thread.sleep((long) wait);
        AtomicReference<Double> avg = this.averageResponseTimes.get(command);
        double thisResponseTime = System.currentTimeMillis() - Long.parseLong(commandBlock[4]);
        double newAvg = avg.updateAndGet(prev -> prev * (requestNum - 1) / requestNum + thisResponseTime / requestNum);
        System.out.printf("Completed request \"%s\". Number of \"%s\" requests: %d. Average Response Time: %.2f ms\n", command, command, requestNum, newAvg);
    }

    public void warningRequest(String[] command){
        outputGUI.warning(command);
    }

    public void levelChangeRequest(String[] command){
        outputGUI.levelChange(command);
    }

    public void reuqestUserDataRequest(String[] command){
        outputGUI.requestUserData(command);
    }

    public void reuqestResearcherData(String[] command){
        outputGUI.requestResearcherData(command);
    }

    public void requestChemicalData(String[] command){
        outputGUI.requestChemicalData(command);
    }

    public void requestHistoryData(String[] command){
        outputGUI.requestHistoryData(command);
    }

    public void gatherForecasterData(String[] command){
        outputGUI.gatherForecasterData(command);
    }

    public void gatherBuoyData(String[] command){
        outputGUI.gatherBuoy(command);
    }

}
